<?php

namespace App\Http\Controllers;

use App\Repositories\WorkerRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    protected $workerRepository;

    public function __construct(WorkerRepository $workerRepository) {
        $this->workerRepository = $workerRepository;
    }

    public function findByName(Request $request) : JsonResponse {
        $name = $request->get('name') ?? null;
        $data = (int) $request->get('data');
        if ($name) {
            if ($data === 0) {
                $worker = $this->workerRepository->findByName($name);
            } else {
                $worker = $this->workerRepository->findDataByName($name);
            }
            if ($worker) {
                return response()->json([
                    'data' => $worker,
                    'status' => 200
                ]);
            }
        }
        return response()->json([
            'data' => 'not found',
            'status' => 204
        ]);
    }
}
