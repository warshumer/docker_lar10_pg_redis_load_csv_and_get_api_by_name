<?php

namespace App\Console\Commands;

use App\Models\Department;
use App\Models\EmploymentType;
use App\Models\JobTitle;
use App\Models\SalaryType;
use App\Models\Worker;
use Illuminate\Console\Command;
use function Psy\debug;

class LoadStartData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:load-start-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Обработка запуска парсинга входного файла
     */
    public function handle()
    {
        $this->parseImport('data.csv');
    }

    /**
     * Функция принимает на вход ассоциативный массив временного хранения
     * класс для сохранения несуществующих в базе вариантов
     * и значение которое небходимо занести в базу
     * в исходных данных присутствует пустой вид занятости в 3х строках данных
     * вовзращает индекс табличного значения из БД
     * @param array $list
     * @param $parameterClass
     * @param string $value
     * @return int
     */
    private function getIfExistsOrSave(array &$list, $parameterClass, string $value) : int {
        $departmentId = $list[$value] ?? null;
        if (!$departmentId) {
            $departmentId = $parameterClass::create(['name' => $value])->id;
            $list[$value] = $departmentId;
        }
        return $departmentId;
    }

    /**
     * Основной парссинг csv с отчисткой данных и последующим заполнением таблицы сотрудников
     * Расзбор входных данных
     * 0 => "Name" - имя сотрудника - worker - поле name
     * 5 => "Typical Hours" - часов работы в неделю - worker - поле typical_hours
     * 6 => "Annual Salary" - годовой заработок - worker - поле annual_salary
     * 7 => "Hourly Rate" - почасовая ставка - worker - поле hourly_rate
     *
     * 1 => "Job Titles" - должность - job_title_id - вынести в таблицу job_titles
     * 2 => "Department" - отдел - department_id - вынести в таблицу departments
     * 3 => "Full or Part-Time" - полный рабочий день или частичная занятость - employment_type_id - вынести в employment_types
     * 4 => "Salary or Hourly" - запрлата или почасовая оплата - salary_type_id - вынести в отдельную salary_types
     * @param string $fileName
     * @return void
     */
    public function parseImport(string $fileName)
    {
        // Очищаем таблицы данных... неизвестно нужно ли думать над тем, что это дополнительные данные, а не базовые
        Department::truncate();
        JobTitle::truncate();
        EmploymentType::truncate();
        SalaryType::truncate();
        Worker::truncate();

        // Инициализируем ассоциативные массивы временного хранения для получения индексов спрочных таблиц
        $departmentsList = [];
        $jobTitlesList = [];
        $employmentTypesList = [];
        $salariesTypesList = [];

        // Открываем файл из папки public на чтение и
        $file = fopen(public_path($fileName), 'r');
        // Считываем заголовок чтобы не делать лишних обработок в цикле
        fgetcsv($file, 200, ",");
        // инициализируем таймер
        $time = floor(microtime(true) * 1000);
        // Массив сбора данных 100 работников для массового заполнения базы
        $workers100 = [];
        // инициализируем даты и время для установки в created_at updated_at
        $dateTime = now();
        $i = 0;
        while (($data = fgetcsv($file, 200, ",")) !==FALSE ) {
            // Сохраняем, собираем ключи справочных таблиц
            $departmentId = $this->getIfExistsOrSave($departmentsList, Department::class, $data[2]);
            $jobTitleId = $this->getIfExistsOrSave($jobTitlesList, JobTitle::class, $data[1]);
            $employmentTypeId = $this->getIfExistsOrSave($employmentTypesList, EmploymentType::class, $data[3]);
            $salaryTypeId = $this->getIfExistsOrSave($salariesTypesList, SalaryType::class, $data[4]);

            // Если есть имя сотрудника
            if ($data[0]) {
                $workers100[] = [
                    'name' => $data[0],
                    'department_id' => $departmentId,
                    'job_title_id' => $jobTitleId,
                    'employment_type_id' => $employmentTypeId,
                    'salary_type_id' => $salaryTypeId,
                    'typical_hours' => ($data[5] !== '') ? (int) $data[5] : null,
                    'annual_salary' => ($data[6] !== '') ? (float) $data[6] : null,
                    'hourly_rate' => ($data[7] !== '') ? (float) $data[7] : null,
                    'created_at' => $dateTime,
                    'updated_at' => $dateTime,
                ];
            }
            $i++;
            // Когда собираем данные на 100 сотрудников вставляем в базу, выводим время работы на этот объём и
            // сбрасываем массив сбора, обнобвляем таймер.
            if ($i % 100 === 0) {
                Worker::insert($workers100);
                $workers100 = [];
                $newTime = floor(microtime(true) * 1000);
                dump("Обработаано {$i} работников. Время - " . ($newTime - $time));
                $time = $newTime;
            }
        }
    }
}
