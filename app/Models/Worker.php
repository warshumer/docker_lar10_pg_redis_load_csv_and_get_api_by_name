<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    use HasFactory;
    // 0 => "Name" - имя сотрудника - worker - поле name
    // 5 => "Typical Hours" - часов работы в неделю - worker - поле typical_hours
    // 6 => "Annual Salary" - годовой заработок - worker - поле annual_salary
    // 7 => "Hourly Rate" - почасовая ставка - worker - поле hourly_rate

    protected $fillable = [
        'department_id',
        'job_title_id',
        'employment_type_id',
        'salary_type_id',
        'name',
        'typical_hours',
        'annual_salary',
        'hourly_salary',
    ];

    public function department()
    {
        return $this->hasOne(Department::class,'id', 'department_id');
    }

    public function jobTitle()
    {
        return $this->hasOne(JobTitle::class,'id', 'job_title_id');
    }

    public function employmentType()
    {
        return $this->hasOne(EmploymentType::class,'id', 'employment_type_id');
    }

    public function salaryType()
    {
        return $this->hasOne(SalaryType::class, 'id', 'salary_type_id');
    }

}
