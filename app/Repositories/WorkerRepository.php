<?php

namespace App\Repositories;

use App\Models\Worker;
use Illuminate\Support\Facades\DB;

class WorkerRepository
{
    protected $worker;

    public function __construct(Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * Пример вывода данных со связями через модель
     * @param string $name
     * @return object|null
     */
    public function findByName(string $name): object|null
    {
        return $this->worker->with([
            'department:id,name', 'jobTitle:id,name', 'employmentType:id,name', 'salaryType:id,name'
        ])->where('name', $name)->first();
    }

    /**
     * Пример вывода данных чистым запросом со связями с таблицами со сборкой без подмоделей, а сразу в чистом виде
     * @param string $name
     * @return object|null
     */
    public function findDataByName(string $name): object|null
    {
        $res = DB::select("
            SELECT w.id, d.name AS department_name, j.name AS job_name , e.name AS employment_name,
                   s.name AS salary_name, w.name AS worker_name, w.typical_hours, w.annual_salary, w.hourly_rate 
            FROM workers w
            LEFT JOIN departments d ON w.department_id = d.id
            LEFT JOIN job_titles j ON w.department_id = j.id
            LEFT JOIN employment_types e ON w.department_id = e.id
            LEFT JOIN salary_types s ON w.department_id = s.id
            WHERE w.name = ? LIMIT 1", [$name]);
        return $res ? $res[0] : null;
    }
}