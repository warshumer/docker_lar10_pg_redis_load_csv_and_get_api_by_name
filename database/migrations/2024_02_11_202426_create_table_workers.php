<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->id();
            $table->integer('department_id');
            $table->integer('job_title_id');
            $table->integer('employment_type_id');
            $table->integer('salary_type_id');
            $table->string('name');
            $table->integer('typical_hours')->nullable();
            $table->decimal('annual_salary', 12, 2)->nullable();
            $table->decimal('hourly_rate', 7, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('workers');
    }
};
