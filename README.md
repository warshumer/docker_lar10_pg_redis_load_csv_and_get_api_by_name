```
cp .env.example .env
```
```
php artisan key:generate
```
```
composer i
```
```
docker network create sail
```
```
change .env DB_HOST to ip address from 
ip addr in command line there u can get ip address of docker network
```
```
docker compose up --build
```
```
php artisan migrate
```
```
php artisan app:load-start-data
```
```
http://localhost/api/worker/find?name=VALLEJO%20JR,%20MARCO&data=0
```
```
http://localhost/api/worker/find?name=VALLEJO%20JR,%20MARCO&data=1
```
